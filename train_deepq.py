import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm

from uno_game.game import Game
from uno_game.players.random_agent import RandomAgent
from uno_game.players.QTableAgent import QTableAgent
from uno_game.players.humans import HumanCLI
from uno_game.players.DQAgent import DQAgent
 
if __name__ == "__main__":
    # initiialize game    
    print("UNO")
    game = Game(id=0)
    game.add_player(player=RandomAgent(id=0,pseudo=f"RandomAgent_{0}",draw_prob=0.25))
    game.add_player(player=RandomAgent(id=1,pseudo=f"RandomAgent_{1}",draw_prob=0.25))
    game.add_player(player=DQAgent(id=2,pseudo=f"DQ_agent{2}",is_training=True,policy_net_filename="3random_0.25/checkpoint_950.pt"))
    
       
    for _ in tqdm(range(1000,2000)): 
        game.start_game()
        game.play(nb_max_iter=10**3)
        game.reset()
        if _ % 50 == 0:
            game.players[2].save(f"3random_0.25/checkpoint_{_}.pt")
            pd.DataFrame(game.players[2].reward_history).to_csv("data/DQAgent/reward_history.csv",index=False,header=False)





    print("\n=======================================================\n")
    print("PRINCIPALES STAT DU JEUX")
    print(f"La durée du jeux: {game.end_time - game.start_time}sec")
    print(f"\nLe jeux est terminé: {game.is_game_over}")
    print("\nNombre de points par joueur:")
    for player in game.players:
        print(f"\t-Pseudo: {player.pseudo} - Nombre de points: {player.total_points} - Nombre d'actions jouées: {player.nb_action_played}")
    
    plt.show()