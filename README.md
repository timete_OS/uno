ENSAI Reinforcement Learning Project: UNO
=========

![alt text](frontend/img/logo_readme.gif)



# UNO: 


Le Uno est un jeu de cartes très populaire dans le monde entier. Il se joue avec un paquet de 108 cartes de différentes couleurs et valeurs. L'objectif du jeu est de se débarrasser de toutes ses cartes en les posant sur une pile au centre de la table.

## Deck
Aujourd'hui nous retrouvons plusieurs decks de uno sur les plateaux de jeux, mais le plus fréquent et aussi officiel est celui décrit dans le tableau ci-dessous décrivant le nombre de carte pour chaque couple (valeur,couleur):

| Type de carte | Rouge | Jaune | Vert | Bleu | Sans couleur |
|--------------|-------|-------|------|------|------|
| Cartes +2     | 2     | 2     | 2    | 2    | 0    |
| Cartes inverser | 2   | 2     | 2    | 2    | 0    |
| Cartes passer | 2    | 2     | 2    | 2    | 0    |
| Cartes joker | 0     | 0     | 0    | 0    | 4    |
| Cartes +4     | 0     | 0     | 0    | 0    | 4    |
| Cartes 0     | 1     | 1     | 1    | 1    | 0    |
| Cartes numérotées (1 à 9) | 2  | 2  | 2 | 2  | 0     |


## Règle du jeux

Parmis les risques diplomatiques, le jeu de UNO fait partie de ceux qui engendrent les tensions les plus élevés comme l'illustre le scénario suivant imaginé par ChatGPT:

>Léo: "Salut tout le monde, c'est Léo ! Qui veut jouer une partie de Uno ?"
>
>Emma  : "Moi, c'est Emma, et je suis partante !"
>
>Max  : "Je m'appelle Max, et je vais tous vous battre !"
>
>Ils jouent quelques tours sans incident, mais tout change quand Emma pose un +2 bleu.
>
>Max : "J'ai un +2 bleu aussi !"
>
>Léo : "Et moi aussi !"
>
>Emma est surprise : "Quoi ? Vous avez tous les deux deux cartes +2 bleus dans votre main ?"
>
>Max et Léo hochent la tête.
>
>Emma : "Attendez, vous pouvez jouer toutes vos cartes +2 bleus d'un coup ?"
>
>Max et Léo se mettent à sourire : "Exactement !"
>
>Emma se sent un peu exclue de la situation et la partie devient moins amicale. Mais quand Max pose une carte de la mauvaise couleur, Léo se met en colère.
>
>Léo : "Eh, tu ne peux pas jouer cette carte, c'est la mauvaise couleur !"
>
>Max : "Ah bon ? Je ne savais pas, je pensais qu'on pouvait jouer n'importe quelle carte si on avait la même valeur !"
>
>Léo est furieux : "Non, ça ne marche pas comme ça ! Tu dois piocher 2 cartes et passer ton tour !"
>
>Max commence à s'agacer : "Mais je ne vois pas pourquoi on ne peut pas jouer les cartes qu'on veut ! C'est n'importe quoi !"
>
>La partie se termine finalement dans une ambiance tendue, et les joueurs se quittent sans se dire au revoir.

Pour éviter ce genre d'incident, nous nous sommes référés aux règles officielles accesible sur Wikipedia

Chaque joueur reçoit 7 cartes et le reste des cartes forme une pioche.
La première carte de la pioche est placée face visible sur la table pour commencer la défausse.

Deux cartes sont compatibles si elles ont la même couleur ou la même valeur ou que la carte que l'on souhaite poser est de type "+4" ou "joker". Si un joueur ne peut pas poser de carte, il doit en piocher une dans la pioche. Un joueur peut également piocher une carte même si il pouvait en jouer une.  Si la carte piochée peut être posée, le joueur peut le faire immédiatement, sinon son tour se termine.  

Les cartes spéciales ont des effets particuliers :
* Les cartes "+2" font piocher deux cartes au joueur suivant et font sauter son tour.
* Les cartes "Inverser" inversent le sens de jeu (de gauche à droite ou de droite à gauche).
* Les cartes "Passer" font passer le tour au joueur suivant.
* Les cartes "+4" font intervenir un challenge: si le joueurs suivant relève le défi et que le joueur qui a posé sa carte avait la possibilité d'en poser une autre hors "+4" et "joker" alors ce dernier pioche 4 carte sinon le joueur suivant pioche 6 cartes et passe son tour; si le joueurs suivant ne relève pas le défi il pioche 4 cartes et passe son tour. Le joueur qui pose "+4" peut changer la couleur demandée à jouer.
* Les cartes Joker permettent au joueur qui la pose de choisir n'importe quelle couleur à jouer.

Le premier joueur à se débarrasser de toutes ses cartes gagne la partie.
Dans le Uno, chaque carte a une valeur de point. Les cartes numérotées de 0 à 9 ont une valeur équivalente à leur numéro. Les cartes spéciales ont également une valeur de point :

Les cartes "Skip", "Reverse" et "+2" ont une valeur de 20 points chacune.
La carte "+4 Wild" a une valeur de 50 points.
La carte "Wild" a une valeur de 50 points.

À la fin d'une partie, les joueurs comptent les points des cartes qu'ils ont encore en main, le but étant d'en avoir le moins.

# Approches
L'objectif de ce projet a donc été d'implémenter un environnement reprenant les règles de jeux du Uno, d'entrainer des agents à jouer à ce jeux et implémenter une interface où humain et agent peuvent jouer. Pour cela, nous avons testé deux approches.

# Q Learning
La première approche que nous avons explorée est celle de  Q-Table Learning. L'algorithme Q-Table Learning est un algorithme Off Policy qui utilise une table de valeurs Q pour stocker les valeurs d'action-état. Il utilise une politique de stratégie greedy pour exploiter l'espace d'action. L'algorithme Q-Table Learning est basé sur la mise à jour de la table de valeurs Q en fonction de l'action effectuée, de la récompense reçue et de la valeur maximale de la table de valeurs Q pour l'état suivant. La formule de mise à jour de la table de valeurs Q est la suivante :

Q(s,a) <- Q(s,a) + alpha * (r + gamma * max(Q(s',a')) - Q(s,a))

Ainsi, et comme dans tous approche d'apprentissage par renforcment, il fut nécéssaire de définir l'espace d'état, d'action et le système de récompense.

## Etats

Pour commencer définissons l'espace d'états. L'ensemble des informations disponibles lorsqu'on joue une partie de uno et nécéssaires opur choisire une action est:
* les cartes que j'ai dans ma main
* la carte au dessus de la pile
* l'ensemble des cartes jouées par moi et mes adversaires
* le nombre de carte de chacun des mes adversaires
* est-ce que je viens de piocher une carte
* est-ce une situation de "+4" challenge

Bien qu'en compte les restrictions lié au deck (i.e. le nombre de type de cartes par couleur), représanter l'ensemble de ces état de manière explicite dans un tableau serait impossible en raison de capacité de mémoire (alors que nous n'avons pas encore évoqué l'espace d'action). C'est pourquoi, nous avons restraint notre espace d'état à:
* la carte au dessus de la pile
* le nombre de cartes du joueurs qui me précède (majoré par 6)
* le nombre de cartes du joueurs qui me succède (majoré par 6)
* est-ce que je viens de piocher une carte
* est-ce une situation de "+4" challenge
* la coleur la plus présente dans ma main

## Actions
De nouveau, expliciter la totalité des actions n'est pas implémentable dans notre cas. C'est pourquoi, nous nous sommes restreint à l'espace d'actions suivant:
* jouer une carte non spéciale de la même couleur que la carte au dessus de la pile
* jouer une carte non spéciale de la valeur que la carte au dessus de la pile mais pas de la même couleur
* jouer une carte +2
* jouer une carte reverse
* jouer une carte skip
* jouer une carte +4 en choisissant la couleur rouge
* jouer une carte +4 en choisissant la couleur bleue
* jouer une carte +4 en choisissant la couleur vert
* jouer une carte +4 en choisissant la couleur jaune
* jouer une carte joker en choisissant la couleur rouge
* jouer une carte joker en choisissant la couleur bleue
* jouer une carte joker en choisissant la couleur vert
* jouer une carte joker en choisissant la couleur jaune
* piocher une carte
* passer mon tour
* accepter le +4 challenge
* refuser le +4 challenge

Toutes ces actions ne sont pas légals en fonction de l'état courant. Par exemple l'action "passer mon tour" n'est légal que si j'ai pioché une carte avant.

## Récomponses
Enfin, concernant le système de récompenses nous avons envisagé plusieurs approches. La première, et la plus courante semble-t-il dans ce genre de situation est la suivant:
* -1 si l'agent perd la partie
* +1 si l'agent gagne la partie
* 0 si la partie n'est pas terminée

Un des challenges qu'implique ce type d'approche est le "sparse reward". Une autre approche plus en lien avec les règles du jeux de uno:

* l'opposé de la somme des points des cartes de la main si l'agent n'a pas gagné la partie
* 1000 si l'agent a gagné la partie

Nous avons testé ces deux système et en se basant sur l'evolution du taux de victoire, il semblerait que le deuxième implique une convergence plus rapide.

## Analyse des résultats

Pour analyser nos résultats, nous avons entrainer notre agent contre 3 autres agents aléatoires que nous avons également implmenté. Toute les 50 parties (aussi appelée épisode) terminées, nous utilisions la matrice Q qui en résultait pour l'évaluer dans un tournoie de 100 partiee afin d'avoir une idée de la courbe d'apprentisage de notre agent. Nous observons les résultats ci-dessous où nous avons représenté l'évolution du taux de victoire sur les 100 parties, le rang moyen de notre agent et son score moyen. 

![alt text](data/graph/q_win.png)
![alt text](data/graph/q_rank.png)
![alt text](data/graph/q_score.png)

Il semblerait qu'au bout de 2000 partie simulées notre agent ait des performance meilleures que notre agent aléatoire

# Deep Q Learning
L'autre approche que nous avons explorée est celle du Deep Q Learning. Le Deep Q-Learning (DQL) est une technique d'apprentissage par renforcement qui utilise des réseaux de neurones profonds pour apprendre la fonction Q-value, qui est une estimation de la valeur d'une action dans un état donné. 
La principale différence entre le Deep Q-Learning et le Q-Table Learning est que le premier utilise une représentation continue de l'espace d'états et d'actions, tandis que le second utilise une table pour stocker les valeurs Q pour chaque état et action possible. Cette différence permet à l'algorithme DQL de fonctionner sur des espaces d'états et d'actions de grande dimension, tandis que l'algorithme Q-Table Learning est limité par la taille de la table nécessaire pour stocker toutes les valeurs Q.

L'algorithme DQL utilise également une technique appelée "experience replay", qui stocke les transitions d'états et d'actions dans une mémoire tampon et utilise un échantillonnage aléatoire de la mémoire pour entraîner le réseau de neurones. Cette technique permet de réduire la corrélation entre les transitions d'états et d'actions, ce qui améliore la stabilité de l'apprentissage.

## Etats
Avec cette approche, nous avons donc été plus exaustive sur la description de l'espace d'état en utilisant pour chaque type de carte (valeur et couleur) un comptage:
* des cartes de ma main
* des cartes jouée depuis le début de la partie
* la carte au dessus de la pioche
* le nombre de cartes du joueurs qui me précède 
* le nombre de cartes du joueurs qui me succède 
* est-ce que je viens de piocher une carte
* est-ce une situation de "+4" challenge

## Actions
L'espace d'action utilisé ici "jouer une carte" pour chaque type de carte (valeur x couleur) ainsi que :
* piocher une carte
* passer mon tour
* accepter le +4 challenge
* refuser le +4 challenge

## Analyse des résultats
Nous avons utilisé le même système d'évaluation que pour l'approche par Q Table. Nous remarquons que DQL semble, pour un même nombre de parties utilisées pour son entrainement, avoir de moins bonnes performance. Cela peut être dû au fait que cette algorithme nécéssite plus de temps d'entrainement.

![alt text](data/graph/dq_win.png)
![alt text](data/graph/dq_rank.png)
![alt text](data/graph/dq_score.png)



# Implémentation
## Environnement et Agents pour le UNO
L'implémentation de l'environnement et des agents se situe dans le dossier uno_game et a été faite en suivant le paradigme de la POO.
Concernant l'implémentation de l'approche Deep Q Learning, nous avons utilisé pytorch en adaptant un de leur tutoriel (https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html) à notre projet.

## Interfaces
Concernant les interfaces, nous avons implmenté deux interfaces permettant à des humains de jouer:
* une interface web utilisant Flask pour le côté backend et pour le coté frontend html,css et javascript (dans le dossier `frontend`)
* une Interface CLI

Les deux interfaces sont accessibles en local en executant `app.py` et l'interface web est également déployée à l'adresse suivante: https://dmkant.github.io/UNO/

# Conclusion


Le jeu de cartes UNO est un environnement complexe pour l'apprentissage par renforcement. Voici quelques-uns des principaux défis que nous avons rencontrés :

* Représentation de l'état : L'état du jeu UNO peut être complexe à représenter, car il peut inclure les cartes des joueurs, la carte visible sur le dessus du paquet, la couleur en cours, le nombre de cartes restantes, etc. Une représentation précise de l'état est essentielle pour que l'agent prenne des décisions optimales.

* Exploration efficace : Comme dans tout environnement d'apprentissage par renforcement, l'exploration de l'espace d'états est essentielle pour trouver une politique optimale. 

* Sparse Reward : Les récompenses dans le jeu UNO sont souvent rares et difficiles à obtenir, car elles ne sont délivrées que lorsqu'un joueur gagne la partie. Cela peut rendre l'apprentissage plus difficile pour l'agent, qui peut avoir du mal à trouver des actions qui améliorent ses chances de gagner. C'est pourquoi nous avons tiré avantage du système de points des cartes pour définir le système de récompense. Nénamoins ce choix a des conséquences sur les actions de l'agent.

* Stratégies adverses : Le jeu UNO est un jeu multi-joueurs, ce qui signifie que l'agent doit être en mesure de prendre en compte les stratégies des autres joueurs pour maximiser ses chances de gagner. Les autres joueurs peuvent avoir des stratégies différentes et imprévisibles, ce qui peut rendre l'apprentissage plus difficile pour l'agent. En l'occurence, nos agents ont été entrainé sur d'autres agents aléatoires mais paramétrisable notament sur la probabilité qu'il tire une carte. Nous avions également pensé à ajouter un hyperparamètre pour définir la proba de jouer un +4 et donc de faire intervenir un +4 challenge