from typing import Optional
from enum import Enum

Color = Enum('Colors', ['RED', 'GREEN', 'BLUE', 'YELLOW'])

CardPoint = {str(value): -value for value in range(10)}
CardPoint.update({
    "+2":-20,
    "+4":-50,
    'choose_color':-50,
    "skip":-20,
    "reverse":-20
})

class Card:

    def __init__(self,card_id:int,value:str, color: Optional[Color]):
        self.color = color
        self.value = str(value)
        self.is_special = value in ["+2","+4",'choose_color',"skip","reverse"]
        self.card_id = card_id
        self.point = CardPoint[str(value)]

    def is_valid(self,other:"Card")->bool:
        # print("is_valid:")
        # print("self",self.to_dict())
        # print("other",other.to_dict())
        # print(self.same_color(other) , self.same_number(other) , other.type == '+4' , other.type == 'choose_color')
        return  self.same_color(other) or self.same_value(other) or other.value == '+4' or other.value == 'choose_color'

    def same_color(self,other:"Card") -> bool:
        if self.color is None:
            print(self.to_dict())
            raise ValueError("Once a card is played, it should ALWAYS have a color")
        if other.color is None:
            return False
        return  self.color == other.color

    def same_value(self, other:"Card") -> bool:
        return self.value == other.value
    
    def set_color(self, color: Color) -> None:
        if not (self.value == '+4' or self.value == 'choose_color'):
            raise ValueError("You can only assign color to +4 and Choose_color")
        self.color = color

    def __lt__(self, other:"Card") -> bool:
        if self.color != other.color:
            colors = {Color.GREEN : 0,
                    Color.YELLOW : 1,
                    Color.RED : 2,
                    Color.BLUE : 3,
                    None : 5}
            return colors[self.color] < colors[other.color]
        elif self.point != other.point :
            return abs(self.point) < abs(other.point)
        else:
            return self.value < other.value
        
    def __str__(self) -> str:
        colors = {Color.GREEN : "\033[1;32m ",
                  Color.YELLOW : "\033[1;33m ",
                  Color.RED : "\033[1;31m ",
                  Color.BLUE : "\033[1;34m "}
        if self.color is not None:
            return f"{colors[self.color]} {self.value} \033[0m"
        else:
            return f"\033[0m  {self.value} \033[0m"

    def __repr__(self) -> str:
        return self.__str__()
    
    def to_dict(self) -> dict:
        match self.color:
            case Color.GREEN:
                color = "GREEN"
            case Color.YELLOW:
                color = "YELLOW"
            case Color.RED:
                color = "RED"
            case Color.BLUE:
                color = "BLUE"
            case other:
                color = "None"
        resu = {
            "color":color,
            "value" : self.value,
            "is_special" : self.is_special,
            "card_id" : self.card_id,
        }
        
        return resu