import os

import random
import numpy as np
import copy
import time
from threading import Thread
from typing import Union,List,Optional

from uno_game.card import Card, Color
from uno_game.players.QTableAgent import QTableAgent
from uno_game.players.player import Player
from uno_game.players.humans import HumanCLI,HumanWebUi
from utils.generator import generateID

global CHOOSE_COLOR, FOUR
FOUR = '+4'
CHOOSE_COLOR = 'choose_color'
TWO = '+2'
SKIP = "skip"
REVERSE = "reverse"


class Game:

    def __init__(self,id:int=0,agent_time_sleep:float=0):
        # Parameterized Attributes
        self.id = id
        self.agent_time_sleep = agent_time_sleep
        
        # Others Attributes
        self.deck = self.generate_deck()
        self.players : List[Player]= []
        self.turn = 0
        self.direction = 1
        self.nb_card_to_draw = 0
    
    # Before Starting 
    def add_player(self,player:Player) -> None:
        self.players.append(copy.deepcopy(player))
        
    def generate_deck(self)->List[Card]:
        deck = []
        card_id = 0        
        for color in Color:
            deck.append(Card(card_id = card_id, color = color, value = 0))
            card_id += 1
            for value in [TWO,SKIP,REVERSE] + list(range(1, 10)):
                for _ in range(2):
                    deck.append(Card(card_id = card_id, color = color, value = value))
                    card_id += 1
        
        # add specials here
        for value in [FOUR,CHOOSE_COLOR]:
            for _ in range(4):
                deck.append(Card(card_id = card_id, color = None, value = value))
                card_id += 1
        # shuffle before returning
        random.shuffle(deck)
        return deck

    def reset(self):
        self.id += 1
        
        # Others Attributes
        self.deck = self.generate_deck()
        for player in self.players:
            player.hand = []
        self.turn = 0
        self.direction = 1
        self.nb_card_to_draw = 0 

    def start_game(self,hand_size:int=7)->None: 
        for player in self.players:
            player.add_cards(self.draw(hand_size))
        
        current_card = self.draw()
        while current_card.is_special:
            current_card = self.draw()
            
        self.current_card = current_card
        self.played_cards:List[Card] = []
        
        self.four_wild_challenge = False
        self.has_drew_card = False
        self.is_game_over = False
        self.is_finished = False
        
        # Game Statistics
        self.start_time = time.time()
    
    def reset_deck(self) -> None:
        deck = copy.deepcopy(self.played_cards)
        random.shuffle(deck)
        self.deck = deck
        self.played_cards:List[Card] = []
        
    # Getter Methods
    def get_player_num(self,player_id:str) -> Optional[int]:
        # Find the player that matches with the ID
        matched_id = np.where([player.id == player_id for player in self.players])[0]
        if len(matched_id) > 0:
            return matched_id[0]

    def check_player_turn(self,player_id:str) -> bool:
        print("check_player_turn")
        player_num = self.get_player_num(player_id=player_id)
        if player_num is None:
            print(f"Aucun joueur avec l'id {player_id}")
            return False
        # Check if it is the player's turn
        elif player_num != self.turn:
            print(f"C'est pas au tour du jouer avec l'id {player_id}: {player_num}-{self.turn}")
        else:
            print("C'est ok magueule tu peux jouer")
            return True
            
    def get_state(self) -> dict:
        adversary = (self.turn - self.direction)%len(self.players)
        state = {"current_card":self.current_card, #Card
                 "four_wild_challenge":self.four_wild_challenge, # bool
                 "has_drew_card":self.has_drew_card,  #bool
                 "played_cards": copy.deepcopy(self.played_cards),
                 "next_player_nb_cards":len(self.players[self.get_next_player_num()].hand),
                 "previus_player_nb_cards":len(self.players[self.get_previus_player_num()].hand),
                 "is_finished":self.is_finished,
                 "is_game_over":self.is_game_over
                 }
    
        return state
    
    def get_previus_player_num(self) -> int:
        player_num = (self.turn - self.direction) % len(self.players)
        return player_num
    
    def get_next_player_num(self) -> int:
        player_num = (self.turn + self.direction) % len(self.players)
        return player_num
        
    # Game General Method
    def draw(self, nb_card:int = 1) -> Union[List[Card],Card]:
        if nb_card > len(self.deck):
            self.reset_deck()
        
        if nb_card < 1 or nb_card > len(self.deck):
            return []
        if nb_card > 1:
            return [self.deck.pop() for _ in range(nb_card)]
        return self.deck.pop()
    
    def next_turn(self)->None:
        self.turn = (self.turn + self.direction) % len(self.players)
    
    def check_winner(self) -> bool:
        empty_hand = [len(player.hand) == 0 for player in self.players]
        return any(empty_hand)
    
    # Game Action
    def play_draw(self) -> Card:
        if self.has_drew_card:
            raise Exception("error: has already drew card")
        
        drew_card = self.draw(nb_card=1)
        # print(f"play_draw: {(drew_card)}")
        if isinstance(drew_card,Card):
            self.players[self.turn].set_drew_card_id(card_id=drew_card.card_id)
            self.players[self.turn].add_cards(card=copy.deepcopy(drew_card))
            self.has_drew_card = True
        else:
            #print("Il n'y as plus assez de cartes")
            self.play_pass_turn()
        
        return drew_card
    
    def play_pass_turn(self) -> None:
        self.has_drew_card = False
        self.four_wild_challenge = False
        self.players[self.turn].set_drew_card_id(None)
        self.next_turn()
    
    def play_contest_four_card(self) -> None:

        if self.players[self.turn].action["contest_four_card"]:
            previus_player_num = self.get_previus_player_num()
            is_bluffing = np.any([self.played_cards[-1].is_valid(card) and card.value not in ["+4","choose_color"] 
                                   for card in self.players[previus_player_num].hand])
            
            if is_bluffing:
                self.players[previus_player_num].add_cards(self.draw(nb_card=4))
            else:
                self.players[self.turn].add_cards(self.draw(nb_card=6))
                self.next_turn() 
        else:
            
            self.players[self.turn].add_cards(self.draw(nb_card=4))
            self.next_turn()
            
        self.has_drew_card = False
        self.four_wild_challenge = False
                
    def play_card(self) -> None:        
        agent_card = self.players[self.turn].action["card_to_play"]
        if agent_card.color is None:
            print("A card to play should ALWAYS have a color")
            raise Exception("A card to play should ALWAYS have a color")
        self.played_cards += [copy.deepcopy(self.current_card)]
        self.players[self.turn].set_drew_card_id(None)
        
        # Update State
        self.current_card = agent_card
        self.has_drew_card = False
        
        self.handle_special_card()
        self.next_turn()    
    
    def handle_special_card(self) -> None:
        # here handle special effects
        match self.current_card.value:
            case "+2":
                self.nb_card_to_draw += 2
                self.next_turn()
                self.players[self.turn].add_cards(self.draw(nb_card=self.nb_card_to_draw))
            case "+4":
                self.nb_card_to_draw += 4
                self.four_wild_challenge = True
                # self.next_turn()
                # self.players[self.turn].add_cards(self.draw(nb_card=self.nb_card_to_draw))
            case "skip":
                self.next_turn()
            case "reverse":
                self.direction *= -1         

    # Agent Run
    def play_agent(self)->int:
        # Check if it is an AI's turn; if so, calls runAI in a new thread
        if self.is_game_over:
            return self.turn
        if self.players[self.turn].is_agent and not self.is_game_over:
            t = Thread(target = self.run_agent)
            t.start()
            t.join()

                       
    def run_agent(self) -> None:
        time.sleep(self.agent_time_sleep)
        self.players[self.turn].policy(self.get_state())        
        self.update_environnement()
        
    def update_environnement(self,webapp_run:bool=True) -> None:
        print("\n=========================================")
        print("Game state", self.get_state())       
        print("Player turn: ",self.turn," - action:", self.players[self.turn].action)
        
        if self.players[self.turn].action["draw_card"] and self.players[self.turn].action["contest_four_card"] is None:
            self.play_draw()
        elif self.players[self.turn].action["contest_four_card"] is not None:
            self.play_contest_four_card()         
        elif self.players[self.turn].action["pass_turn"]:
            self.play_pass_turn() 
        elif self.players[self.turn].action["card_to_play"] is not None:
            self.play_card()    
        else:
            raise ValueError("Run Agent: Probleme avec les action")
        self.nb_card_to_draw = 0
        
        if self.check_winner():
            self.is_game_over = True
            self.end_time = time.time()
            for i in range(len(self.players)):
                self.players[i].update_total_points()
        else:
            if webapp_run:
                self.play_agent()
    
    def play(self,nb_max_iter:int = np.inf) -> int:
        iter = 1
        while not self.is_game_over and iter <= nb_max_iter:
            i = self.turn
            # print(f"\nAu tour {i}, ou jour avec {self.players[i].pseudo} - voici sa main : {self.players[i].hand}")
            self.players[i].policy(self.get_state())
            self.update_environnement(webapp_run=False)
            iter += 1
        self.is_finished = True
        
        self.end_time = time.time()
        for i in range(len(self.players)):
            self.players[i].update_total_points()
            self.players[i].update(state=self.get_state())
        
        return {p.pseudo : p.total_points for p in self.players}
    
    def save_agents_state(self, path):
        for player in self.players:
            player.save(os.path.join(path, player.pseudo))
