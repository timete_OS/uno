from uno_game.card import Card,Color
from uno_game.players.player import Player

from typing import List,Union,Optional
import copy
import numpy as np

    
class HumanCLI(Player) :
    
    def __init__(self, id: int,pseudo:str="Human", hand: List[Card]=[]):
        super().__init__(id=id,pseudo=pseudo, hand=hand,is_agent=False)


    def select_color(self):
        colors = {Color.GREEN : "\033[1;32m ",
                    Color.YELLOW : "\033[1;33m ",
                    Color.RED : "\033[1;31m ",
                    Color.BLUE : "\033[1;34m "}

        is_valid_color = False
        color_selected = None
        while not is_valid_color:
            print(",".join([f"{color_code} {color_name.name} \033[0m" for color_name,color_code in colors.items()]))
            chosen_color = input("Choose the index of a color : \n")
            
            try:
                chosen_color = int(chosen_color)
            except ValueError:
                print("Please type a valid index !")
                continue
                        
            if chosen_color in range(4):
                is_valid_color = True
                color_selected = list(colors.keys())[chosen_color]
            else:
                print("Please type a valid index !")
        return color_selected
    
    def play_card(self,current_card:Card)-> bool:
        
        l = len(self.hand)
        mask = self.possible_actions(current_card)          
        
        chosen = input("Choose the index of a card : \n")
        try:
            chosen = int(chosen)
        except ValueError:
            print("Please type a valid index !")
            return False
        
        if chosen in range(l) and mask[chosen]:
            card_selected = self.hand.pop(chosen) 
            
            if card_selected.value in ["+4","choose_color"]:
                color_selected = self.select_color()
                card_selected.set_color(color_selected)
                
            self.action["card_to_play"] = card_selected
            return True
        else:
            print("not a possible action")
            return False


    def play_four_wild_challenge(self) -> None:

        print(f"Le joueur qui vous précède a joué une carte +4\n")
        valid_answer = False
        is_bluffing = None
        
        while not valid_answer:
            chosen = input(" Pensez-vous qu'il pouvait jouer une autre carte ? (yes/no)\n")
            if chosen in ["yes","no"]:
                is_bluffing = chosen == "yes"
                valid_answer = True
            else:
                print("not a possible action")
                        
        self.action["contest_four_card"] = is_bluffing
    
    def play_drew_card(self,current_card:Card)-> None:
        drew_card_num = self.get_card_num(card_id=self.drew_card_id)
        print(f"Voici la carte que vous avez pioché: {self.hand[drew_card_num]}")
        
        #Cas où la catre n'est pas jouable: seule action possible est de passer son tour
        if not current_card.is_valid(self.hand[drew_card_num]):
            print("Cette carte n'est pas jouable")
            self.action["pass_turn"] = True
        # Si la carte est jouable le joueur a le choix entre la jouer ou passer son tour
        else: 
            #On demande au joueur si il veut jouer cette carte  
            valid_answer = False        
            while not valid_answer:
                chosen = input("Voulez-vous jouer cette carte ? (yes/no)\n")
                
                if chosen in ["yes","no"]:
                    valid_answer = True
                else:
                    print("not a possible action")
            # En fonction de sa reponse on maj son action 
            if chosen == "no":
                self.action["pass_turn"] = True 
            else:
                card_selected = self.hand.pop(drew_card_num) 
                if card_selected.value in ["+4","choose_color"]:
                    color_selected = self.select_color()
                    card_selected.set_color(color_selected)
                self.action["card_to_play"] = card_selected
                                                 
    def policy(self, state):
        # Common to ALL AGENT
        self.reset_action()
        self.nb_action_played += 1
        
        print(f"Current card is : {state['current_card']}\n")
        self.hand.sort()
        print("Les cartes de votre main sont:",self.hand)
        
        # Le joueur précédent a joué un +4        
        if state["four_wild_challenge"]:
            self.play_four_wild_challenge()
        # Le joueur vient de piocher une carte => soit il l'a joue soit il passe son tour    
        elif state["has_drew_card"]:
            self.play_drew_card(current_card=state["current_card"])        
        else:
            # soit pioche une carte soit joue une carte               
            
            #On demande au joueur si il veut jouer une carte  ou piocher
            valid_answer = False        
            while not valid_answer:
                chosen = input("Voulez-vous piocher une carte ? (yes/no)\n")
                
                if chosen not in ["yes","no"]:
                    print("not a possible action")
                else:
                    if chosen == "yes":
                        self.action["draw_card"] = True
                        valid_answer = True
                    else:
                        valid_answer = self.play_card(current_card=state["current_card"])
        
        # Common to ALL AGENT           
        self.drew_card_id = None


        
    def train_policy(self, state: dict):
        return super().train_policy(state)

    def possible_actions(self, card: Card, second_round = False):
        mask = [card.is_valid(h) for h in self.hand]
        return mask

class HumanWebUi(Player) :
    
    def __init__(self, id: int,pseudo:str="Human", hand: List[Card]=[]):
        super().__init__(id=id,pseudo=pseudo, hand=hand,is_agent=False)

    def policy(self, state):
        pass

    def human_select_color(self, card_id:str, color:str) -> None:
        card_num = self.get_card_num(card_id=card_id)
        if card_num is not None:
            self.hand[card_num].set_color(Color[color])
        else:
            print("error: card not in player's hand")
            return "error: card not in player's hand"

    def human_play_card(self,card_id:str,current_card:Card)->str:

        # Check if the player actually has the card
        card_num = self.get_card_num(card_id)
        if card_num is not None:
            selected_card = self.hand[card_num]
        else:
            print("human_play_card")
            print(card_num)
            print(self.hand)
            return "error: card not in player's hand"
        
        self.reset_action()
        # Check if the card matches with the current top card
        if current_card.is_valid(selected_card):
            self.remove_card(selected_card)
            self.action["card_to_play"] = copy.deepcopy(selected_card)
            return "success"
        else:
            return "error: cards do not match"

    def human_play_draw(self) -> None:        
        self.reset_action()
        self.action["draw_card"] = True

    def human_play_pass_turn(self) -> None:        
        self.reset_action()
        self.action["pass_turn"] = True

    def human_play_four_wild_challenge(self,is_bluffing:bool) -> None:        
        self.reset_action()
        self.action["contest_four_card"] = is_bluffing