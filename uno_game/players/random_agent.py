import numpy as np
import random 
import copy
from typing import List,Optional,Union

from uno_game.players.player import Player,Action
from uno_game.card import Card,Color

class RandomAgent(Player):

    def __init__(self, id:int,pseudo:str="AI", hand:List[Card]=[],draw_prob:float=0):
        super().__init__(id=id,pseudo=pseudo, hand=hand,is_agent=True)
        self.draw_prob = draw_prob
    
    def policy(self, state:dict) -> None: 
        
        # Common to ALL AGENT
        self.reset_action()
        self.nb_action_played += 1
        
        # Specific to RandomAgent
        self.is_valid_state(state=state,raise_exception=True)
        
        if state["four_wild_challenge"]:
            contest_four_card = random.choice([True,False])
            self.action["contest_four_card"] = contest_four_card
        
        else:
            draw_prob = 0 if state["has_drew_card"] else self.draw_prob
            draw_card = np.random.binomial(n=1,p=draw_prob,size=1)
            
            valid_cards_index = [i for i in range(len(self.hand))  if state["current_card"].is_valid(self.hand[i]) ]
            
            if state["has_drew_card"]:
                drew_card_num = self.get_card_num(card_id=self.drew_card_id)
                valid_cards_index = [drew_card_num] if drew_card_num in valid_cards_index else []
            
            if draw_card or (len(valid_cards_index) == 0 and not state["has_drew_card"]):
                self.action["draw_card"] = True
                
            elif len(valid_cards_index) > 0:
                agent_card = self.hand.pop(random.choice(valid_cards_index))
                if  (agent_card.value == '+4' or agent_card.value == 'choose_color'):
                    player_color = [card.color for card in self.hand if card.color is not None]
                    color = random.choice(player_color) if len(player_color) > 0 else random.choice(list(Color))
                    agent_card.set_color(color)
                
                self.action["card_to_play"] = copy.deepcopy(agent_card)
            
            else:
                self.action["pass_turn"] = True 
        
        # Common to ALL AGENT        
        self.drew_card_id = None

