import numpy as np
import random 
from typing import List
import itertools
import copy
import math
from collections import namedtuple, deque


import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


from uno_game.players.player import Player, Action
from uno_game.card import Card, Color



def compare_models(model_1_parameters, model_2_parameter):
    for p1, p2 in zip(model_1_parameters,model_2_parameter):
        if p1.data.ne(p2.data).sum() > 0:
            return False
    return True

BATCH_SIZE = 128
GAMMA = 0.99
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 1000
TAU = 0.005
LR = 1e-4

# if GPU is to be used
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

class DQN(nn.Module):

    def __init__(self, n_observations, n_actions):
        super(DQN, self).__init__()
        self.layer1 = nn.Linear(n_observations, 128)
        self.layer2 = nn.Linear(128, 128)
        self.layer3 = nn.Linear(128, n_actions)

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        return self.layer3(x)


class DQAgent(Player):

    ValueDict = {str(value): value for value in range(10)}
    ValueDict.update({
        "+2":10,
        "skip":11,
        "reverse":12,
        "+4":13,
        "choose_color":14,
    })

    ColorDict = {Color.RED:0,Color.GREEN:1,Color.BLUE:2,Color.YELLOW:3}

    ValueRevertDict = {value:key for key,value in ValueDict.items()}
    ColorRevertDict = {value:key for key,value in ColorDict.items()}

    ActionDict = {(value,color):i for i,(value,color) in enumerate(itertools.product(list(ValueDict.keys()),list(ColorDict.keys())))}    
    ActionDict.update({value: 60 +i for i,value in enumerate(["contest_four_card_true","contest_four_card_false","draw_card","pass_turn"])})
    ActionRevertDict = {value:key for key,value in ActionDict.items()}

    def __init__(self, id:int,pseudo:str="AI", hand:List[Card]=[],is_training:bool = False,policy_net_filename:str=None):
        # important to keep in memory for update of the Qtable
        super().__init__(id=id,pseudo=pseudo, hand=hand,is_agent=True)
        self.is_training = is_training
        self.epsilon = 0.9
        
        n_actions = len(self.ActionDict)
        n_observations = 176

        self.policy_net = DQN(n_observations, n_actions).to(device)
        self.target_net = DQN(n_observations, n_actions).to(device)
        
        if policy_net_filename is not None:
            self.policy_net.load_state_dict(torch.load(f'data/DQAgent/{policy_net_filename}'))
            
        self.target_net.load_state_dict(self.policy_net.state_dict())

        self.optimizer = optim.AdamW(self.policy_net.parameters(), lr=LR, amsgrad=True)
        self.memory = ReplayMemory(10000)
        
        self.steps_done = 0
        self.previous_action = None
        self.reward_history = []
    
    def save(self,filename:str) -> None:
        torch.save(self.policy_net.state_dict(), f'data/DQAgent/{filename}')
    
    def read(self,policy_net_filename:str)->None:
        
        old_model_param = self.policy_net.parameters()
        self.policy_net.load_state_dict(torch.load(f'data/DQAgent/{policy_net_filename}'))
        self.target_net.load_state_dict(self.policy_net.state_dict())
    
    def optimize_model(self):
        if len(self.memory) < BATCH_SIZE:
            return
        transitions = self.memory.sample(BATCH_SIZE)
        # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
        # detailed explanation). This converts batch-array of Transitions
        # to Transition of batch-arrays.
        batch = Transition(*zip(*transitions))

        # Compute a mask of non-final states and concatenate the batch elements
        # (a final state would've been the one after which simulation ended)
        non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                            batch.next_state)), device=device, dtype=torch.bool)
        non_final_next_states = torch.cat([s for s in batch.next_state
                                                    if s is not None])
        state_batch = torch.cat(batch.state)
        action_batch = torch.cat(batch.action)
        reward_batch = torch.cat(batch.reward)

        # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
        # columns of actions taken. These are the actions which would've been taken
        # for each batch state according to policy_net
        state_action_values = self.policy_net(state_batch).gather(1, action_batch)

        # Compute V(s_{t+1}) for all next states.
        # Expected values of actions for non_final_next_states are computed based
        # on the "older" target_net; selecting their best reward with max(1)[0].
        # This is merged based on the mask, such that we'll have either the expected
        # state value or 0 in case the state was final.
        next_state_values = torch.zeros(BATCH_SIZE, device=device)
        with torch.no_grad():
            next_state_values[non_final_mask] = self.target_net(non_final_next_states).max(1)[0]
        # Compute the expected Q values
        expected_state_action_values = (next_state_values * GAMMA) + reward_batch

        # policy_net_state_old = self.policy_net.parameters()
        # Compute Huber loss
        criterion = nn.SmoothL1Loss()
        loss = criterion(state_action_values, expected_state_action_values.unsqueeze(1))

        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        # In-place gradient clipping
        torch.nn.utils.clip_grad_value_(self.policy_net.parameters(), 1000)
        self.optimizer.step()          
    
    def possible_actions(self,state:dict) -> np.ndarray:
        nb_actions = len(self.ActionDict)
        mask = np.zeros(nb_actions) - np.inf

        if state["four_wild_challenge"]:
            # index correspondant à contest & not contest +4 challenge
            mask[self.ActionDict["contest_four_card_false"]] = 0
            mask[self.ActionDict["contest_four_card_true"]] = 0

        elif state["has_drew_card"]:
            mask[self.ActionDict["pass_turn"]] = 0
            drew_card_num = self.get_card_num(card_id=self.drew_card_id)
            
            if state["current_card"].is_valid(self.hand[drew_card_num]):
                drew_card_value = self.hand[drew_card_num].value
                
                if drew_card_value in ['+4','choose_color']:
                    for color in list(self.ColorDict.keys()):
                        mask[self.ActionDict[(drew_card_value,color)]] = 0  
                else:
                    drew_card_color = self.hand[drew_card_num].color
                    mask[self.ActionDict[(drew_card_value,drew_card_color)]] = 0  
                
            
        else:
            valid_cards = [card for card in self.hand if state["current_card"].is_valid(card)]
            if len(valid_cards) == 0:
                mask[self.ActionDict["draw_card"]] = 0
            for card in valid_cards:
                card_value = card.value
                if card_value in ['+4','choose_color']:
                    for color in list(self.ColorDict.keys()):
                        mask[self.ActionDict[(card_value,color)]] = 0
                else:
                    card_color = card.color
                    mask[self.ActionDict[(card_value,card_color)]] = 0 

        return mask  
    

    def cards_to_tensor(self,list_cards:List[Card],separet_wild:bool=True) -> torch.Tensor:
        
        if separet_wild:
            dict_array_shape = (len(self.ColorDict),len(self.ValueDict)-2)
            dim_hot = np.product(dict_array_shape) + 2
        else:
            dict_array_shape = (len(self.ColorDict),len(self.ValueDict))
            dim_hot = np.product(dict_array_shape)        

        list_card_ind = []

        for card in list_cards:
            if card.value not in ["+4","choose_color"] or not separet_wild:
                card_coord = (self.ColorDict[card.color],self.ValueDict[card.value])
                list_card_ind.append(np.ravel_multi_index(card_coord,dict_array_shape))
            elif card.value == "+4":
                list_card_ind.append(52)
            elif card.value == "choose_color":
                list_card_ind.append(53)
            else:
                raise ValueError("Probleme avec les valeurs de la cartes")

        # Transformation en 1hot pour chacune des carte puis sum sur l'axe 0 pour avoir le comptage
        if len(list_card_ind) == 0:
            return torch.zeros(dim_hot,dtype=torch.float32,device=device)
        else:
            return F.one_hot(torch.tensor(list_card_ind,device=device),num_classes=dim_hot).sum(0).float()

    def state_to_tensor(self,state) -> torch.Tensor:
        tensor_hand = self.cards_to_tensor(list_cards = self.hand,separet_wild=True)
        tensor_played_card = self.cards_to_tensor(list_cards = state["played_cards"],separet_wild=False)
        tensor_current = self.cards_to_tensor(list_cards =[ state["current_card"]],separet_wild=False)
        tensor_number_card = torch.tensor([state["previus_player_nb_cards"],state["next_player_nb_cards"]],device=device)

        # Concate all
        tensor_state = torch.cat([tensor_hand,tensor_played_card,tensor_current,tensor_number_card])
        tensor_state = tensor_state.reshape((1,tensor_hand.shape[0] + tensor_played_card.shape[0] + tensor_current.shape[0] + tensor_number_card.shape[0]))
        
        return tensor_state
    
    def policy(self, state:dict) -> Action:
        # Common to ALL AGENT
        self.reset_action()
        self.nb_action_played += 1
        
        if self.is_training:
            self.training_policy(state)
        else:
            self.ready_policy(state)
        
        # Common to ALL AGENT        
        self.drew_card_id = None
        
        return self.action

    def index_to_action(self,index:int) -> None:
        
        action = self.ActionRevertDict[index]
        
        if action == "contest_four_card_true":
            self.action["contest_four_card"] = True
        elif action == "contest_four_card_false":
            self.action["contest_four_card"] = False
        elif action == "draw_card":
            self.action["draw_card"] = True
        elif action == "pass_turn":
            self.action["pass_turn"] = True
        else:
            action_value,action_color = action[0],action[1]
            if action_value in ["+4","choose_color"]:
                
                valid_cards_index = [index for index, card in enumerate(self.hand) if card.value == action_value]
                agent_card = self.hand.pop(random.choice(valid_cards_index))
                agent_card.set_color(action_color)
                
            else:
                valid_cards_index = [index for index, card in enumerate(self.hand) if card.value == action_value and card.color == action_color]
                random_index = random.choice(valid_cards_index)
                agent_card = self.hand.pop(random_index)

            self.action["card_to_play"] = copy.deepcopy(agent_card)
    
    def select_action(self,tensor_state:torch.Tensor,mask:np.ndarray) -> torch.Tensor:
        sample = random.random()
        
        if sample > self.epsilon  or not self.is_training:
            with torch.no_grad():
                output = self.policy_net(tensor_state) + torch.tensor(mask,device=device)
                return output.max(1)[1].view(1, 1)
        else:
            valid_index = np.where(mask != -np.inf)[0]
            random_selection = np.random.choice(valid_index)
            return torch.tensor([[random_selection]], device=device, dtype=torch.long)

    def ready_policy(self,state) -> Action:
        tensor_state = self.state_to_tensor(state)
        
        mask = self.possible_actions(state)
        action = self.select_action(tensor_state=tensor_state,mask=mask)
        self.index_to_action(index=action.item())
      
    def training_policy(self,state) -> Action:
        # Convert
        tensor_state = self.state_to_tensor(state)
        
        if self.previous_action is not None:
            self.update(state=state) 
        
        #update 
        mask = self.possible_actions(state)
        action = self.select_action(tensor_state=tensor_state,mask=mask)
        
        
        self.index_to_action(index=action.item())
        
        self.previous_action = action
        self.previous_state = tensor_state

    def update(self,state:dict):
        self.update_net(state=state)
        
    def update_net(self,state):
        if self.is_training:
            tensor_state = self.state_to_tensor(state)
            self.update_total_points()
            if state["is_finished"]:
                reward = self.total_points if self.total_points != 0 else 10**3
                current_state = None
                self.reward_history.append(reward)
                self.steps_done += 1
            else:
                reward = self.total_points
                current_state = tensor_state
            
            reward = torch.tensor([reward], device=device)   
            
            # Store the transition in memory
            self.memory.push(self.previous_state, self.previous_action, current_state, reward)
            self.optimize_model()
            # Move to the next state
            # state = next_state

            # Perform one step of the optimization (on the policy network)
            # optimize_model()

            # Soft update of the target network's weights
            # θ′ ← τ θ + (1 −τ )θ′
            target_net_state_dict = self.target_net.state_dict()
            policy_net_state_dict = self.policy_net.state_dict()
            for key in policy_net_state_dict:
                target_net_state_dict[key] = policy_net_state_dict[key]*TAU + target_net_state_dict[key]*(1-TAU)
            self.target_net.load_state_dict(target_net_state_dict)

