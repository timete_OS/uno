import os
import random

import numpy as np

from typing import List, Optional

from uno_game.players.player import Player, Action
from uno_game.card import Card, Color




class QTableAgent(Player):

    def __init__(self, id: int, pseudo: str = "AI", qtable_file: Optional[str] = None, hand: List[Card] = []):
        # important to keep in memory for update of the Qtable
        super().__init__(id=id, pseudo=pseudo, hand=hand, is_agent=True)
        self.nb_actions = 17

        if qtable_file:
            self.qtable = np.load(file=qtable_file)
        else:
            self.qtable = np.random.random((78000, self.nb_actions))

        self.previous_state_index = None
        self.previous_action_index = None
        self.previous_reward = 0
        self.epsilon = .2
        self.is_training = False
        self.alpha = .2
        self.nb_action_played = 0

    def index_to_action(self, index, card: Card) -> bool:
        match index:
            case 0:
                # same color
                # here, don't exclude same number
                # because it should be able to play a yellow one on a yellow one
                possibles = [index for index, c in enumerate(self.hand) if card.same_color(
                    c) and not c.is_special]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
            case 1:
                # same number
                possibles = [index for index, c in enumerate(self.hand) if card.same_value(c) and not c.is_special and not card.same_color(c)]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
            case 2:
                # +2
                possibles = [index for index, c in enumerate(
                    self.hand) if c.value == "+2"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
            case 3:
                # +4 Yellow
                possibles = [index for index, c in enumerate(self.hand) if c.value == "+4"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.YELLOW
            case 4:
                # +4 Red
                possibles = [index for index, c in enumerate(self.hand) if c.value == "+4"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.RED
            case 5:
                # +4 Blue
                possibles = [index for index, c in enumerate(self.hand) if c.value == "+4"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.BLUE
            case 6:
                # +4 Green
                possibles = [index for index, c in enumerate(self.hand) if c.value == "+4"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.GREEN 
            case 7:
                # +4 Yellow
                possibles = [index for index, c in enumerate(self.hand) if c.value == "choose_color"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.YELLOW
            case 8:
                # +4 Red
                possibles = [index for index, c in enumerate(self.hand) if c.value == "choose_color"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.RED
            case 9:
                # +4 Blue
                possibles = [index for index, c in enumerate(self.hand) if c.value == "choose_color"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.BLUE
            case 10:
                # +4 Green
                possibles = [index for index, c in enumerate(self.hand) if c.value == "choose_color"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
                self.action["card_to_play"].color = Color.GREEN                                                          

            case 11:
                # Skip
                possibles = [index for index, c in enumerate(
                    self.hand) if c.value == "skip"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
            case 12:
                # Reverse
                possibles = [index for index, c in enumerate(
                    self.hand) if c.value == "reverse"]
                chosen_index = random.choice(possibles)
                self.action["card_to_play"] = self.hand.pop(chosen_index)
            case 13:
                # Draw Card
                self.action["draw_card"] = True
            case 14:
                self.action["contest_four_card"] = True
            case 15:
                # pass turn
                self.action["contest_four_card"] = False
            case 16:
                self.action["pass_turn"] = True

    def possible_actions(self, card: Card, state: dict):
        mask = np.ones(self.nb_actions) - np.inf
        hand = self.hand.copy()

        if state["four_wild_challenge"]:
            mask[14:16] = 0
            return mask

        if state["has_drew_card"]:
            mask[16] = 0
            hand = [c for c in hand if c.card_id == self.drew_card_id]
        else:
            mask[13] = 0 
        special_to_index = {"+2": [2], "+4": [3,4,5,6],"choose_color": [7,8,9,10], "skip": [11], "reverse": [12]}
        
        for h in hand:
            # print(h,"possible_actions")
            if card.same_color(h) and not h.is_special:
                mask[0] = 0
            if card.same_value(h) and not card.same_color(h) and not h.is_special:
                mask[1] = 0
            if h.is_special  and card.is_valid(h):
                for ind in special_to_index[h.value]:
                    mask[ind] = 0
        self.nb_action_played +=1
        return mask

    def state_to_index(self, state):
        possiblity_number = {'current_card': 108,
                             'four_wild_challenge':  2,
                             'has_drew_card': 2,
                             'next_player_nb_cards': 6,
                             'previus_player_nb_cards': 6,
                             'color_mode_hand':5}
        index = 0
        base = 1
        for key, p in reversed(possiblity_number.items()):
            if key == 'current_card':
                index += state[key].card_id * base
            elif key in ['next_player_nb_cards','previus_player_nb_cards']:
                index += min(state[key],6) * base
            elif key == "color_mode_hand":
                colors = [card.color.value for card in self.hand if card.color is not None]
                if len(colors) > 0:
                    colors_unique, colors_counts = np.unique(colors,return_counts=True)
                    index += colors_unique[np.argmax(colors_counts)]* base
                else:
                    index += 5* base
            else:
                index += state[key] * base
            base *= p

        return index

    def policy(self, state) -> Action:
        self.reset_action()
        self.nb_action_played += 1

        card = state['current_card']
        mask = self.possible_actions(card, state)
        index = self.state_to_index(state)
        


        if (np.random.rand() < self.epsilon) and self.is_training:
            action = np.random.choice(np.arange(self.nb_actions)[mask == 0])
        else:
            action = (self.qtable[index, :] + mask).argmax()
            
        self.update_qtable((self.qtable[index, :] + mask).max())
        
        self.previous_action_index = action
        self.previous_state_index = index
        
        self.index_to_action(action, card)

        return self.action

    def update_qtable(self, maximum, y=1):
        self.update_total_points()
        if len(self.hand) == 0:
            reward = 10**3
        else:
            reward = self.total_points 
        s = self.previous_state_index
        a = self.previous_action_index

        tmp = self.qtable[s, a]
        self.qtable[s, a] += self.alpha * (reward + y * maximum - tmp)

    def update(self,state):
        self.update_total_points()
        if len(self.hand) == 0:
            reward = 10**3
        else:
            reward = self.total_points 
        
        s = self.previous_state_index
        a = self.previous_action_index

        tmp = self.qtable[s, a]
        self.qtable[s, a] += self.alpha * (reward  - tmp)

    def save(self, filename):
        np.save(filename, self.qtable)