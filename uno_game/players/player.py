from uno_game.card import Card,Color

from typing import List,Union,Optional,TypedDict
import numpy as np


class Action(TypedDict):
    card_to_play: Optional[Card]
    draw_card: Optional[bool]
    contest_four_card: Optional[bool]
    pass_turn: Optional[bool]


class Player:
    def __init__(self, id : int,pseudo:str, hand : List[Card]=[],is_agent:bool=False):
        # Parameterized Attributes
        self.id = id
        self.pseudo = pseudo
        self.hand = hand
        self.is_agent = is_agent
        
        # Others Attributes
        self.drew_card_id = None
        self.total_points = 0
        self.action : Action = {
            "card_to_play":None,
            "draw_card":None,
            "contest_four_card":None,
            "pass_turn": None
        }
        self.nb_action_played = 0

    def policy(self, state:dict):
        pass

    def train_policy(self, state:dict):
        pass

    def save(self, filename:str):
        # save informations of the model
        pass

    def update(self,state:dict):
        pass
    

    def possible_actions(self, card: Card, second_round = False):
        pass

    def add_cards(self,card:Union[List[Card],Card])->None:
        if type(card) is not list:
            card = [card]
        self.hand.extend(card)
    
    def remove_card(self,card:Card)->None:
        self.hand = [player_card for player_card in self.hand if player_card.card_id != card.card_id]

    def get_card_num(self, card_id:Union[str,int]) -> Optional[int]:
        card_num = [i for i in range(len(self.hand)) if str(self.hand[i].card_id) == str(card_id)]
        if len(card_num) > 0:
            return card_num[0]
        else:
            print(f"Aucune carte avec comme id {card_id} est dans la main du joueur")
            print(f"voici les card du joeur {[(card,card.card_id) for card in self.hand]}")
            return None
    
    def set_drew_card_id(self,card_id:Union[str,int]) -> None:
        self.drew_card_id = str(card_id)
    
    def reset_action(self) -> None:
        self.action = {
            "card_to_play":None,
            "draw_card":None,
            "contest_four_card":None,
            "pass_turn":None,
        }
        
    def is_valid_state(self,state:dict,raise_exception:bool=False)->bool:
        necessary_keys = [ "four_wild_challenge", "has_drew_card", "current_card" ]
        
        if type(state) is not dict:
            if raise_exception:
                raise ValueError("""L'etat n'est pas valide,cela devrait être un dictionnaire qui contient au moins les clés suivante: 
                             'four_wild_challenge', 'has_drew_card', 'current_card'""")
            else:
                return False
        elif any([key not in state.keys() for key in necessary_keys]):
            if raise_exception:
                raise ValueError("""L'etat n'est pas valide,cela devrait être un dictionnaire qui contient au moins les clés suivante: 
                             'four_wild_challenge', 'has_drew_card', 'current_card'""")
            else:
                return False
        else:
            return True
    
    def update_total_points(self) -> None:
        self.total_points = np.sum([card.point for card in self.hand])
        