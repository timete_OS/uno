import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class Singleton(type):
    """
    -> https://refactoring.guru/fr/design-patterns/singleton
    The Singleton class can be implemented in different ways in Python. Some
    possible methods include: base class, decorator, metaclass. We will use the
    metaclass because it is best suited for this purpose.
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

class SmtpClient(metaclass=Singleton):
    def __init__(self) -> None:

        self._config = {
                "OUTLOOK": {
                    "MAIL_ADRESS" : "uno-ensai@outlook.fr",
                    "PASSWORD": "Pmdelkinc132!csoiIjk",
                    "HOST": "smtp.office365.com", 
                    "PORT": 587
                }
                }

    def envoie_mail(
        self,
        mail_destinateur: str,
        objet: str = "",
        message: str = "",
        name_server: str = "OUTLOOK",
    ) -> int:

        config = self._config[name_server]
        expediteur = f"""UNO ENSAI <{config["MAIL_ADRESS"]}>"""

        msg = MIMEMultipart()
        msg["From"] = expediteur
        msg["To"] = mail_destinateur
        msg["Subject"] = objet

        msg.attach(MIMEText(message, "html"))

        try:
            server = smtplib.SMTP(config["HOST"], config["PORT"])
            server.ehlo()
            server.starttls()
            server.login(config["MAIL_ADRESS"], config["PASSWORD"])
            text = msg.as_string()
            server.sendmail(expediteur, mail_destinateur, text)
            server.quit()
            est_envoye = True
        except Exception as e:
            print(e)
            est_envoye = False

        return est_envoye

if __name__ == "__main__":
    smtp_outlook = SmtpClient()
    a = smtp_outlook.envoie_mail(mail_destinateur="hugo.miccinilli@insa-rennes.fr")
    print(a)