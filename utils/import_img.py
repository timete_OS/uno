colors = ["red","yellow","blue","green","black"]
values = ["reverse","buy-2","block","change-color","buy-4"] + [str(i) for i in range(0,10)]

import requests

for color in colors:
    for value in values:
        print(color,value)
        if color == "black" and value not in ["change-color","buy-4"]:
            pass
        else:
            img_data = requests.get(f"https://unapy.guilherr.me/assets/cards/{value}/{color}.svg").content
            with open(f'frontend/img/cards/{color}/{value}.svg', 'wb') as handler:
                handler.write(img_data)