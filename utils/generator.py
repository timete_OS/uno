import random
import string 

def generateID(l,prev):
    # Generates a random string of length l
    # that is not in prev
    res = ""
    tokens = string.digits + string.ascii_lowercase + string.ascii_uppercase
    while not(res) and not(res in prev):
        res = ''.join(random.choice(tokens) for _ in range(l))
    return res