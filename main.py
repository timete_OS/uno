import os

import pandas as pd

from tqdm import tqdm

from uno_game.game import Game
from uno_game.players.random_agent import RandomAgent
from uno_game.players.QTableAgent import QTableAgent
from uno_game.players.humans import HumanCLI
from uno_game.players.DQAgent import DQAgent
 
if __name__ == "__main__":
    # initiialize game    
    print("UNO")

    

    # Training games Games
    def train(desc:str, training_rounds=100):
        for agent in game.players:
            agent.is_training = True
        for _ in tqdm(range(training_rounds),desc):
            game.start_game()
            game.play()
            game.reset()
        for agent in game.players:
            agent.is_training = False

    def generate_data(game:Game, filename:str):

        results = {
            'round' : [],
            'agentName' : [],
            'score' : [],
            "episode" : [],
        }
        for round in range(40):
            print(round, end = '\r')
            train(desc=f"Entrainement {round:02}/20", training_rounds=50)
            for _ in range(100):
                game.start_game()
                scores = game.play(nb_max_iter=10**3)
                for name, points in scores.items():
                    results['round'].append(round*50)
                    results['episode'].append(_)
                    results['agentName'].append(name)
                    results['score'].append(points)
                game.reset()
            game.save_agents_state(path = 'data')
                
            df = pd.DataFrame(results)
            df.to_csv(filename, index=False)


    game = Game(id=0)
    
    # Add Agent
    nb_random_agent = 3
    game.add_player(QTableAgent(id = nb_random_agent + 1, pseudo='Qtable'))
    for i in range(nb_random_agent):
        game.add_player(player=RandomAgent(id=i,pseudo=f"RandomAgent_{i}",draw_prob=0.25))
    


    generate_data(game, os.path.join("data", "qtableresults.csv"))

    # print("Playing Random vs Random")
    # game = Game(id=1)
    # # Add Agent
    # nb_random_agent = 4
    # for i in range(nb_random_agent):
    #     game.add_player(player=RandomAgent(id=i,pseudo=f"RandomAgent_{i}",draw_prob=0.25))
    
    
    # generate_data(game, os.path.join("data", "randoms.csv"))
    # # 
    # print("\n=======================================================\n")
    # print("PRINCIPALES STAT DU JEUX")
    # print(f"La durée du jeux: {game.end_time - game.start_time}sec")
    # print(f"\nLe jeux est terminé: {game.is_game_over}")
    # print("\nNombre de points par joueur:")
    # for player in game.players:
        # print(f"\t-Pseudo: {player.pseudo} - Nombre de points: {player.total_points} - Nombre d'actions jouées: { }")