
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import random
import copy
from typing import TypedDict,Dict

from uno_game.game import Game
from uno_game.card import Card, Color
from uno_game.players.QTableAgent import QTableAgent
from uno_game.players.random_agent import RandomAgent
from uno_game.players.DQAgent import DQAgent
from uno_game.players.player import Player
from uno_game.players.humans import HumanWebUi,HumanCLI

from utils.generator import *
from utils.smtp_outlook import *



app = Flask(__name__)
CORS(app, support_credentials=True)

# Sets of all player and game IDs in use
used_player_ids = set()
used_game_ids = set()

# Games that are currently in waiting room
waiting_games:Dict[str,Game] = {}
# Games that are currently playing a game
running_games:Dict[str,Game] = {}

# Length of game and player IDs
id_length = 12

@app.route("/")
def index():
    return '''
    <h1>UNO Game</h1>
    <h2><i>Flask API</i></h2>
    '''
''' GAME CREATION AND JOINING '''


@app.route("/new_user",methods=["POST"])
# ADD A PLAYER TO A NEW GAME
def newUser():
    # Get player name
    city = request.get_json(force=True)["city"]
    ip = request.get_json(force=True)["ip"]
    # smtp_outlook = SmtpClient()
    # smtp_outlook.envoie_mail(mail_destinateur="hugo.miccinilli@eleve.ensai.fr",
    #                          objet = "Un utilisateur de UNO",
    #                          message = f"IP: {ip} <br> Ville: {city}",)
    
    return "sucess"

@app.route("/create",methods=["POST"])
# ADD A PLAYER TO A NEW GAME
def createGame():
    # Get player name
    player_name = request.get_json(force=True)["name"]

    # Generate random IDs
    game_id = generateID(id_length, used_game_ids)
    player_id = generateID(id_length, used_player_ids)
    
    used_game_ids.add(game_id)
    used_player_ids.add(player_id)

    # Initialize game object    
    game = Game(id=game_id,agent_time_sleep=2)
    game.add_player(HumanWebUi(id=player_id,pseudo=player_name))
    waiting_games[game_id] = game
    
    print("Created game [",game_id,"], player id [",player_id,"], player name [",player_name,"]")
    
    return jsonify({"id":player_id, "game":game_id})

@app.route("/join",methods=["POST"])
# ADD A PLAYER TO AN EXISTING GAME
def joinGame():
    # Get player name and game ID
    json = request.get_json(force=True)
    player_name = json["name"]
    game_id = json["game"]

    # Generate random ID
    player_id = generateID(id_length, used_player_ids)
    used_player_ids.add(player_id)

    # Add player to game object
    if game_id in waiting_games:
        # Check that player name is unique
        if player_name in [waiting_games[game_id].players[i].pseudo for i in range(len(waiting_games[game_id].players))]:
            return "error: player name already in use"
        waiting_games[game_id].add_player(HumanWebUi(id=player_id,pseudo=player_name))
    else:
        return "error: nonexistent game"
    print("Add player [",player_name,"], id [",player_id,"], to game [",game_id,"]")
    return jsonify({"id":player_id, "game":game_id})
 

''' WAITING ROOM '''
@app.route("/waiting_room",methods=["GET","POST"])
# GET CURRENTLY CONNECTED PLAYERS, AND WHETHER GAME HAS STARTED.
def lobby():
    # Get game ID
    game_id = request.args["game"]
    try:
        player_id = request.args["id"]
    except:
        print("warning: player id not supplied in call to /waiting_room")
        player_id = False

    # Get player info
    if game_id in waiting_games: # If the game hasn't started yet, it will be in waiting_games.
        # Determine if player has been kicked
        kicked = "no"
        if player_id:
            if not (player_id in [waiting_games[game_id].players[i].id for i in range(len(waiting_games[game_id].players))]):
                kicked = "yes"
        # Return data
        return jsonify({
            "players":str([player.pseudo for player in waiting_games[game_id].players]),
            "start":"no",
            "kicked":kicked
        })
    elif game_id in running_games: # If the game has started, it will be in running_games.
        return jsonify({
            "players":str([player.pseudo for player in running_games[game_id].players]),
            "start":"yes",
            "kicked":"no"
        })
    else:
        return "error: nonexistent game"

@app.route("/remove",methods=["POST"])
# Kick a player from the game
def kick():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_num = json["num"] # number of the player to kick
    player_id = json["id"]

    if game_id in waiting_games:
        if waiting_games[game_id].players[0].id == player_id:
            if waiting_games[game_id].players[player_num].id != player_id:
                removed_player_name = waiting_games[game_id].players[player_num].id
                waiting_games[game_id].players.pop(player_num)
                return "Successfully removed player "+removed_player_name
            return "Tu ne peux pas te supprimer toi-même"    
        return "Seul le maitre du jeu peut supprimer des joueurs"
    
    return "error: nonexistent game"

@app.route("/addAI",methods=["POST"])
# ADD AN AI TO THE GAME.
def addAI():
    # Get game ID
    json = request.get_json(force=True)
    game_id = json["game"]
    agent = json["agent"]

    # Check if game is valid
    if game_id in waiting_games:
        ai_id = generateID(id_length, used_player_ids)
        if agent == "RandomAgent":
            waiting_games[game_id].add_player(RandomAgent(id=ai_id,pseudo=f"RandomAI_{ai_id}",draw_prob=0.25))
        elif agent == "QAgent":
            waiting_games[game_id].add_player(QTableAgent(id=ai_id,pseudo=f"QAgent_{ai_id}"))
        elif agent == "DQAgent":
            waiting_games[game_id].add_player(DQAgent(id=ai_id,pseudo=f"DQAgent_{ai_id}",policy_net_filename="notraining.pt"))
    else:
        return "error: nonexistent game"
    return "success"

''' GAME '''

@app.route("/start",methods=["POST"])
# START THE GAME
def startGame():
    # Get game ID
    json = request.get_json(force=True)
    game_id = json["game"]

    # Check if game is valid
    if game_id in waiting_games:
        # Move game from waiting_games to running_games
        running_games[game_id] = waiting_games[game_id]
        waiting_games.pop(game_id)
        # Initialize game state
        running_games[game_id].start_game()
        print(running_games)
        return "success"
    
    # Errors (not displayed to client, but useful for debugging)
    elif game_id in running_games:
        return "error: game already running"
    return "error: nonexistent game"

@app.route("/player_data",methods=["GET"])
# SEND PLAYER NAME AND TURN NUMBER
def getPlayerData():
    game_id = request.args["game"]
    player_id = request.args["id"]
    if game_id in running_games:
        player_num = running_games[game_id].get_player_num(player_id)
        player = running_games[game_id].players[player_num]
        
        resu = {"name":str(player.pseudo),
                "turn_number":str(player_num)}
        print(f"getPlayerData {resu}")
        return jsonify(resu)
        
    return "error: game not found"

@app.route("/data",methods=["GET"])
# SEND ALL GAME DATA TO PLAYERS
def getData():
    # Get game and player ID
    game_id = request.args["game"]
    player_id = request.args["id"]
    
    if game_id in running_games:
        player_num = running_games[game_id].get_player_num(player_id)
        player = running_games[game_id].players[player_num]
        player.hand.sort()
        
        player_cards = []
        for card in player.hand:
            card_dict = card.to_dict()
            is_valid = running_games[game_id].current_card.is_valid(card)
            if running_games[game_id].has_drew_card:
                is_valid = is_valid and str(card.card_id) == str(player.drew_card_id)
                
            card_dict.update({"is_valid":is_valid})
            player_cards += [card_dict]
        
        top_card_dict = running_games[game_id].current_card.to_dict()
        top_card_dict.update({"is_valid":True})
        
        
        resu = {
                "player_cards":player_cards,
                "top_card": top_card_dict,
                "turn":running_games[game_id].turn,
                "all_player_cards":[{"pseudo":player.pseudo,
                                     "cards":[card.to_dict() for card in player.hand]}
                                    for player in running_games[game_id].players],
                
                "has_drew_card":running_games[game_id].has_drew_card,
                "four_wild_challenge":running_games[game_id].four_wild_challenge,
                } 
        
        return jsonify(resu)
    
    return "error: game not found"

@app.route("/play",methods=["POST"])
def playCard():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_id = json["id"]
    card_id = json["card"]

    if game_id in running_games:
        print("playCard",player_id, card_id)
        if running_games[game_id].check_player_turn(player_id=player_id):
            player_num = running_games[game_id].get_player_num(player_id=player_id)
            player:HumanWebUi = running_games[game_id].players[player_num]
            player.human_play_card(current_card=copy.deepcopy(running_games[game_id].current_card),
                                card_id=card_id)
            
            running_games[game_id].update_environnement()
    return "error: game not found"

@app.route("/draw_card",methods=["POST"])
# ADD ONE CARD TO PLAYER'S HAND, ADVANCE TURN
def drawCard():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_id = json["id"]
  
    if game_id in running_games:
        print("drawCard")
        if running_games[game_id].check_player_turn(player_id=player_id):
            player_num = running_games[game_id].get_player_num(player_id=player_id)
            player:HumanWebUi = running_games[game_id].players[player_num]
            player.human_play_draw()
            running_games[game_id].update_environnement()
    return "error: game not found"
        
@app.route("/pass_turn",methods=["POST"])
# ADD ONE CARD TO PLAYER'S HAND, ADVANCE TURN
def passTurn():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_id = json["id"]
    
    if game_id in running_games:
        print("passTurn")
        if running_games[game_id].check_player_turn(player_id=player_id):
            player_num = running_games[game_id].get_player_num(player_id=player_id)
            player:HumanWebUi = running_games[game_id].players[player_num]
            player.human_play_pass_turn()
            
            running_games[game_id].update_environnement()
    return "error: game not found"

@app.route("/select_color",methods=["POST"])
# ADD ONE CARD TO PLAYER'S HAND, ADVANCE TURN
def selectColor():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_id = json["id"]
    card_id = json["card_id_selected"]
    color = json["color_selected"]
    
    
    if game_id in running_games:
        print("selectColor")
        if running_games[game_id].check_player_turn(player_id=player_id):     
            player_num = running_games[game_id].get_player_num(player_id=player_id)
            running_games[game_id].players[player_num].human_select_color(card_id=card_id,color=color)
            running_games[game_id].players[player_num].human_play_card(current_card=copy.deepcopy(running_games[game_id].current_card),
                                                                       card_id=card_id)
            running_games[game_id].update_environnement()
            
    return "error: game not found"

@app.route("/four_wild_challenge",methods=["POST"])
def playFourWildChallenge():
    json = request.get_json(force=True)
    game_id = json["game"]
    player_id = json["id"]
    is_bluffing = json["is_bluffing"]

    if game_id in running_games:
        print("four_wild_challenge",player_id, is_bluffing)
        if running_games[game_id].check_player_turn(player_id=player_id):
            player_num = running_games[game_id].get_player_num(player_id=player_id)
            player:HumanWebUi = running_games[game_id].players[player_num]
            player.human_play_four_wild_challenge(is_bluffing=is_bluffing)
            
            running_games[game_id].update_environnement()
    return "error: game not found"

@app.route("/classement",methods=["GET"])
# SEND ALL GAME DATA TO PLAYERS
def getClassement():
    # Get game and player ID
    game_id = request.args["game"]
    player_id = request.args["id"]
    
    if game_id in running_games:
        resu = []
        print("getClassement: ",len(running_games[game_id].players))
        for i in range(len(running_games[game_id].players)):
            running_games[game_id].players[i].update_total_points()
            resu += [{"pseudo":str(running_games[game_id].players[i].pseudo),
                    "total_points":str(running_games[game_id].players[i].total_points)}]
        
        return jsonify(resu)
    
    return "error: game not found"

if __name__ == "__main__":
    choice=input("Quelle interface voulez-vous utiliser ? \n\n Tapez '0' pour utiliser l'interface web\n Tapez '1' pour utiliser La CLI\n")
    while choice not in ["0","1"]:
        choice=input("Mauvaise réponse.\nQuelle interface voulez-vous utiliser ? \n\n Tapez '0' pour utiliser l'interface web\n Tapez '1' pour utiliser La CLI\n")
    
    if choice == "0":
        import webbrowser
        import os
        webbrowser.open(f"{os.getcwd()}/frontend/index.html")
        app.run(host="localhost",port=8000)
    else:
        game = Game(id=0)
        game.add_player(QTableAgent(id=0,pseudo="QTABLE"))
        game.add_player(QTableAgent(id=1,pseudo="QTABLE"))
        game.add_player(HumanCLI(id=3,pseudo="Joueur Humain"))
        
        game.start_game()
        game.play()
