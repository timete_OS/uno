import os

import pandas as pd

from tqdm import tqdm

from uno_game.game import Game
from uno_game.players.random_agent import RandomAgent
from uno_game.players.QTableAgent import QTableAgent
from uno_game.players.humans import HumanCLI
from uno_game.players.DQAgent import DQAgent


import re

if __name__ == "__main__":
    # initiialize game    
    print("UNO")    

    game = Game(id=0)

    # Add Agent
    
    nb_random_agent = 3
    game.add_player(DQAgent(id = nb_random_agent + 1, pseudo='DQAgent',policy_net_filename="notraining.pt"))
    for i in range(nb_random_agent):
        game.add_player(player=RandomAgent(id=i,pseudo=f"RandomAgent_{i}",draw_prob=0))
    
    

    results = {
            'round' : [],
            "episode" : [],
            'agentName' : [],
            'score' : []
        }
    
    rounds = [int(re.search(r'\d+',file).group()) for file in os.listdir("data/DQAgent/3random_0.25/")]
    sorted(rounds)
    
    for round in sorted(rounds):
        print(round)
        game.players[0].read(f"3random_0.25/checkpoint_{round}.pt")
        
        
        for episode in tqdm(range(100)):
            game.start_game()
            scores = game.play(nb_max_iter=10**3)
            for name, points in scores.items():
                results['round'].append(round)
                results['episode'].append(episode)
                results['agentName'].append(name)
                results['score'].append(points)
            game.reset()
            
        df = pd.DataFrame(results)
        df.to_csv("data/dqresults.csv", index=False)